﻿using System;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }

    [SerializeField] private GameObject m_StartCanvas;
    [SerializeField] private GameObject m_PlayCanvas;
    [SerializeField] private GameObject m_DeadCanvas;
    [SerializeField] private GameObject m_WinCanvas;
    [SerializeField] private GameObject m_LoseCanvas;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        // Just in case, security check
        m_StartCanvas.SetActive(true);
        m_PlayCanvas.SetActive(false);
        m_DeadCanvas.SetActive(false);
        m_WinCanvas.SetActive(false);
        m_LoseCanvas.SetActive(false);
    }

    public void HideStartCanvas()
    {
        m_StartCanvas.SetActive(false);
    }

    public void TogglePlayCanvas(bool status)
    {
        m_PlayCanvas.SetActive(status);
    }

    public void DisplayDeadCanvas()
    {
        m_DeadCanvas.SetActive(true);
    }

    public void DisplayWinCanvas()
    {
        m_WinCanvas.SetActive(true);
    }

    public void DisplayLoseCanvas()
    {
        m_LoseCanvas.SetActive(true);
    }
}