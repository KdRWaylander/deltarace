﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UISkinButton : MonoBehaviour
{
    [SerializeField] private GameObject m_SkinPanel;
    [SerializeField] private GameObject m_TapToStartText;
    [SerializeField] private GameObject m_LevelText;

    private bool m_IsOn;

    private void Start()
    {
        m_IsOn = false;
        m_SkinPanel.SetActive(false);
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject(0))
            return;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began && m_IsOn)
                ToggleSkinPanel();
        }
    }

    public void ToggleSkinPanel()
    {
        m_IsOn = !m_SkinPanel.activeSelf;

        m_SkinPanel.SetActive(m_IsOn);

        m_TapToStartText.SetActive(!m_IsOn);
        m_LevelText.SetActive(!m_IsOn);

        GameManager.Instance.ToggleSkinShopGameState(m_IsOn);
    }
}