﻿using UnityEngine;
using UnityEngine.UI;

public class UIGemSkinButton : MonoBehaviour
{
    [SerializeField] private int m_SkinIndex;
    [SerializeField] private Image m_SkinImage;
    [SerializeField] private Image m_LockImage;

    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
    }

    private void Start()
    {
        UpdateGemSkinButton(m_SkinIndex);
    }

    private void OnEnable()
    {
        SkinManager.Instance.NewSkinUnlocked += UpdateGemSkinButton;
    }

    public void SetSkin()
    {
        SkinManager.Instance.SetSkin(m_SkinIndex);
    }

    private void UpdateGemSkinButton(int index)
    {
        if (index != m_SkinIndex) // security check
            return;

        if (SkinManager.Instance.IsSkinUnlocked(m_SkinIndex))
        {
            m_SkinImage.gameObject.SetActive(true);
            m_SkinImage.sprite = SkinManager.Instance.GetSkinPreview(m_SkinIndex);
            m_LockImage.gameObject.SetActive(false);

            m_Button.interactable = true;
        }
        else
        {
            m_SkinImage.gameObject.SetActive(false);
            m_LockImage.gameObject.SetActive(true);

            m_Button.interactable = false;
        }
    }

    private void OnDisable()
    {
        SkinManager.Instance.NewSkinUnlocked -= UpdateGemSkinButton;
    }
}