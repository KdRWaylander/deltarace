﻿using UnityEngine;
using UnityEngine.UI;

public class Pin : MonoBehaviour
{
    [SerializeField] private Transform m_Player;
    [SerializeField] private Image m_PinHeadImage;

    private RectTransform m_RectTransform;

    private void Awake()
    {
        m_RectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        m_PinHeadImage.sprite = m_Player.GetComponent<SkinController>().Skin.previewSprite;
    }

    private void FixedUpdate()
    {
        if (m_Player.position.z >= 400)
            gameObject.SetActive(false);

        m_RectTransform.localPosition = new Vector3((m_Player.position.z / LevelGenerator.Instance.LevelLength) * 690 - 345, m_RectTransform.localPosition.y, m_RectTransform.localPosition.z);
        // multiplied by 690 because progress bar is 700px wide with 5px padding on each side
        // minus 345 (=690/2) because progress bar is centered on x=0
    }
}