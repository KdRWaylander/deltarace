﻿using UnityEngine;

public class UIStars : MonoBehaviour
{
    [SerializeField] private GameObject[] m_Stars;

    private void Start()
    {
        int nbStars = 5 - GameManager.Instance.Rank;
        for(int i = 0; i < m_Stars.Length; i++)
        {
            m_Stars[i].SetActive(i < nbStars);
        }
    }
}