﻿using UnityEngine;

public class UISettingsButton : MonoBehaviour
{
    [SerializeField] private GameObject m_SettingsPanel;
    
    private bool m_IsOpen = false;

    private void Start()
    {
        m_SettingsPanel.SetActive(false);
    }

    public void ToggleSettingsPanel()
    {
        m_IsOpen = !m_SettingsPanel.activeSelf;
        m_SettingsPanel.SetActive(m_IsOpen);

        GameManager.Instance.ToggleSkinShopGameState(false); // needed when the player goes straight from skin shop to settings
    }
}