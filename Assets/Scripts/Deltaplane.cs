﻿using UnityEngine;

public class Deltaplane : MonoBehaviour
{
    public void Dive(float rotationSpeed, float maxTilt)
    {
        transform.rotation = Quaternion.Euler(Mathf.Min(maxTilt, transform.rotation.eulerAngles.x + rotationSpeed * Time.deltaTime), 0, 0);
    }

    public void Stabilize(float rotationSpeed)
    {
        transform.rotation = Quaternion.Euler(Mathf.Max(0, transform.rotation.eulerAngles.x - rotationSpeed * Time.deltaTime), 0, 0);
    }

    public void Fly(float horizontalSpeed, float verticalSpeed, float minHeight, float maxHeight)
    {
        transform.Translate((Vector3.forward * horizontalSpeed + transform.forward * horizontalSpeed + transform.up * verticalSpeed) * Time.fixedDeltaTime, Space.World);
        
        // if deltaplane is in the fly zone's superior half
        transform.position = (transform.position.y >= (maxHeight - minHeight) / 2f) ?
            new Vector3(transform.position.x, Mathf.Min(transform.position.y, maxHeight), transform.position.z) : // if below flying height ceiling, take actual y position, else take ceiling position
            new Vector3(transform.position.x, Mathf.Max(transform.position.y, minHeight), transform.position.z); // same but inverted for flying height floor
    }
}