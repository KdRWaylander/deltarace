﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Deltaplane))]
public class PlayerController : MonoBehaviour
{
    public bool IsUnderDestructionBoost { get { return m_IsUnderDestructionBoost; } }
    
    [SerializeField] private float m_MinHorizontalSpeed = 3f;
    [SerializeField] private float m_MaxHorizontalSpeed = 10f;
    [SerializeField] private float m_DiveAcceleration = 5f;
    [SerializeField] private float m_AscensionSpeed = 2f;
    [SerializeField] private float m_RotationSpeed = 50f;
    [SerializeField] private float m_FlyingHeightFloor = -20f;
    [SerializeField] private float m_FlyingHeightCeiling = 20f;
    [SerializeField] private float m_MaxTilt = 75f;
    [SerializeField] private AnimationCurve m_MaxTiltCurve;

    private bool m_IsDiving;
    private bool m_IsUnderDestructionBoost;
    private float m_ForwardSpeed;
    private float m_UpSpeed;
    private float m_FormerMinHorizontalSpeed;

    private Deltaplane m_Deltaplane;
    private Magnet m_Magnet;

    private void Awake()
    {
        m_Deltaplane = GetComponent<Deltaplane>();
        m_Magnet = GetComponentInChildren<Magnet>();
    }

    private void Start()
    {
        m_ForwardSpeed = m_MinHorizontalSpeed;
        m_UpSpeed = m_AscensionSpeed;
        m_FormerMinHorizontalSpeed = m_MinHorizontalSpeed;

        m_Magnet.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (GameManager.Instance.GameState != GameState.Play)
            return;
        
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_IsDiving = true;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            m_IsDiving = false;
        }
        #else
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                m_IsDiving = true;
            }

            if(touch.phase == TouchPhase.Ended)
            {
                m_IsDiving = false;
            }
        }
        #endif
    }

    private void FixedUpdate()
    {
        if (GameManager.Instance.GameState != GameState.Play)
            return;
        
        if (m_IsDiving)
        {
            float localMaxTilt = m_MaxTilt * m_MaxTiltCurve.Evaluate((transform.position.y + 10) / (m_FlyingHeightCeiling - m_FlyingHeightFloor));
            m_Deltaplane.Dive(m_RotationSpeed, localMaxTilt);

            if(transform.position.y > m_FlyingHeightFloor + 1)
            {
                m_ForwardSpeed = Mathf.Min(m_ForwardSpeed + m_DiveAcceleration * Time.fixedDeltaTime, m_MaxHorizontalSpeed);
            }
            else
            {
                m_ForwardSpeed = Mathf.Max(m_ForwardSpeed - m_DiveAcceleration * Time.deltaTime, m_MinHorizontalSpeed);
            }

            m_UpSpeed = 0f;

        }
        else
        {
            if (transform.rotation.eulerAngles.x > 0)
                m_Deltaplane.Stabilize(m_RotationSpeed);

            m_ForwardSpeed = Mathf.Max(m_ForwardSpeed - m_DiveAcceleration * Time.fixedDeltaTime * 1.25f, m_MinHorizontalSpeed);

            // Boundary lock
            m_UpSpeed = (transform.position.y <= m_FlyingHeightCeiling) ? Mathf.Min(m_UpSpeed + m_AscensionSpeed * Time.fixedDeltaTime, m_AscensionSpeed): 0;
        }

        m_Deltaplane.Fly(m_ForwardSpeed, m_UpSpeed, m_FlyingHeightFloor, m_FlyingHeightCeiling);
    }

    public void ResetAllBoost()
    {
        m_IsUnderDestructionBoost = false;
        m_Magnet.gameObject.SetActive(false);
        m_MinHorizontalSpeed = m_FormerMinHorizontalSpeed;
        GameManager.Instance.HandleBoosterExpiredOrCancelled();
    }

    public void SpeedBoost(float quantity, float duration)
    {
        GameManager.Instance.HandleNewBoosterPicked(BoosterType.Speed);
        StartCoroutine(SpeedCoroutine(quantity, duration));
    }

    public void MagnetBoost(float duration)
    {
        GameManager.Instance.HandleNewBoosterPicked(BoosterType.Magnet);
        StartCoroutine(MagnetCoroutine(duration));
    }

    public void DestructionBoost()
    {
        GameManager.Instance.HandleNewBoosterPicked(BoosterType.Destruction);
        m_IsUnderDestructionBoost = true;
    }

    private IEnumerator SpeedCoroutine(float quantity, float duration)
    {
        m_FormerMinHorizontalSpeed = m_MinHorizontalSpeed;

        m_MinHorizontalSpeed = Mathf.Min(m_MinHorizontalSpeed + quantity, m_MaxHorizontalSpeed);
        yield return new WaitForSeconds(duration);
        m_MinHorizontalSpeed = m_FormerMinHorizontalSpeed;

        GameManager.Instance.HandleBoosterExpiredOrCancelled();
    }

    private IEnumerator MagnetCoroutine(float duration)
    {
        m_Magnet.gameObject.SetActive(true);
        yield return new WaitForSeconds(duration);
        m_Magnet.gameObject.SetActive(false);

        GameManager.Instance.HandleBoosterExpiredOrCancelled();
    }
}