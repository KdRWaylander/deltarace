﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator Instance { get; private set; }

    public float LevelLength { get { return m_LevelLength; } }
    
    [SerializeField] private GameObject[] m_ObstaclesPrefabs;
    [SerializeField] private GameObject m_GemPrefab;
    [SerializeField] private float m_StepBetweenObstacles = 20f;
    [SerializeField] private int m_LowerSpawnBoundary = -5;
    [SerializeField] private int m_HigherSpawnBoundary = 15;
    [SerializeField] private float m_LevelLength = 400f;

    private List<Obstacle> m_Obstacles;

    public Action<List<Obstacle>> TerrainGenerated;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GenerateLevel();
    }

    public void GenerateLevel() // Wrapped in a function, just in case it needs to be called from GameManager or else instead of Start
    {
        StartCoroutine(GenerateLevelCoroutine());
    }

    private IEnumerator GenerateLevelCoroutine()
    {
        m_Obstacles = new List<Obstacle>();

        // Handle obstacles placement
        bool isComplete = false;
        while(isComplete == false)
        {
            GameObject obstacle = Instantiate(m_ObstaclesPrefabs[UnityEngine.Random.Range(0, m_ObstaclesPrefabs.Length)], transform);
            m_Obstacles.Add(obstacle.GetComponent<Obstacle>());

            obstacle.transform.localPosition = new Vector3(0, UnityEngine.Random.Range(m_LowerSpawnBoundary, m_HigherSpawnBoundary), m_StepBetweenObstacles * (m_Obstacles.Count));
            obstacle.transform.localRotation = Quaternion.identity;

            // true when the max quantity of obstacles is placed while leaving enough (1.5*Step) place between the last obstacle and the finish
            isComplete = m_Obstacles[m_Obstacles.Count - 1].transform.position.z >= m_LevelLength - m_StepBetweenObstacles * 1.5f;

            yield return null; // One instanciation per frame to reduce the load and create an apparition effect
        }

        // Handle gems placement
        for(int i = 0; i < m_Obstacles.Count - 1; i++)
        {
            Vector3 dir = Vector3.Normalize(m_Obstacles[i+1].transform.position - m_Obstacles[i].transform.position);
            Vector3 pos = new Vector3(0, (m_Obstacles[i].transform.position.y + m_Obstacles[i+1].transform.position.y) / 2f, (m_Obstacles[i].transform.position.z + m_Obstacles[i + 1].transform.position.z) / 2f);
            
            for(int j = -2; j < 3; j++)
            {
                Instantiate(m_GemPrefab, pos + dir * 2 * j, Quaternion.identity, transform);
            }

            yield return null;
        }

        HandleTerrainGenerated(m_Obstacles);
    }

    public void HandleTerrainGenerated(List<Obstacle> obstacles)
    {
        if (TerrainGenerated != null)
            TerrainGenerated(obstacles);
    }
}