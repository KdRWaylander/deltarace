﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemManager : MonoBehaviour
{
    public static GemManager Instance { get; private set; }

    public int TotalGems { get { return m_TotalGems; } }
    public int CurrentGems { get { return m_CurrentGems; } }
    public float MultiplierGems { get; private set; }

    [SerializeField] private int m_FirstPlaceGemMultiplier = 10;
    [SerializeField] private int m_SecondPlaceGemMultiplier = 5;
    [SerializeField] private int m_ThirdPlaceGemMultiplier = 2;
    [SerializeField] private int m_FourthPlaceGemMultiplier = 1;

    private int m_CurrentGems;
    private int m_TotalGems;

    public event Action NewGemPicked;
    public event Action NewGemsAmount;

    private const string TOTAL_GEMS = "TOTAL_GEMS";

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        // Check total gems collected
        if (PlayerPrefs.HasKey(TOTAL_GEMS))
            m_TotalGems = PlayerPrefs.GetInt(TOTAL_GEMS);
        else
            PlayerPrefs.SetInt(TOTAL_GEMS, 0);

        m_CurrentGems = 0;
    }

    public void CashInGems(int rank)
    {
        int multiplier = 1;
        switch (rank)
        {
            case 1:
                multiplier = m_FirstPlaceGemMultiplier;
                break;
            case 2:
                multiplier = m_SecondPlaceGemMultiplier;
                break;
            case 3:
                multiplier = m_ThirdPlaceGemMultiplier;
                break;
            case 4:
                multiplier = m_FourthPlaceGemMultiplier;
                break;
        }

        m_TotalGems += m_CurrentGems * multiplier;
        MultiplierGems = multiplier;
        PlayerPrefs.SetInt(TOTAL_GEMS, m_TotalGems);
    }

    public void AddGems(int amount)
    {
        m_TotalGems += amount;
        PlayerPrefs.SetInt(TOTAL_GEMS, m_TotalGems);
        HandleNewGemsAmount();
    }

    public void WithdrawGems(int amount)
    {
        m_TotalGems -= amount;
        PlayerPrefs.SetInt(TOTAL_GEMS, m_TotalGems);
        HandleNewGemsAmount();
    }

    public void HandleNewGemPicked()
    {
        m_CurrentGems += 1;
        if (NewGemPicked != null)
            NewGemPicked();
    }

    public void HandleNewGemsAmount()
    {
        if (NewGemsAmount != null)
            NewGemsAmount();
    }
}