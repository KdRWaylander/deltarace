﻿using UnityEngine;

public class SceneryManager : MonoBehaviour
{
    [SerializeField] private Material m_GreyMaterial;
    [SerializeField] private Material m_RedMaterial;
    [SerializeField] private Material m_YellowMaterial;

    private void Start()
    {
        int level = Mathf.Abs(GameManager.Instance.Level); // Absolute value just in case a negative int comes in
        int index = Mathf.FloorToInt(level / 10) % 3; // Every 10 levels, a rotation on 3 different skins

        switch (index)
        {
            case 0:
                ChangeMaterialInAllChildren(m_GreyMaterial);
                break;
            case 1:
                ChangeMaterialInAllChildren(m_RedMaterial);
                break;
            case 2:
                ChangeMaterialInAllChildren(m_YellowMaterial);
                break;
            default:
                break;
        }
    }

    private void ChangeMaterialInAllChildren(Material mat)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).tag == "Cliff")
                transform.GetChild(i).GetComponent<MeshRenderer>().material = mat;
        }
    }
}