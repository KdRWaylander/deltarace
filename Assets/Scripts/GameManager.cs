﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public GameState GameState { get; private set; }
    public PlayerController Player { get { return m_Player; } }
    public int Level { get { return m_Level; } }
    public int Rank { get; private set; }

    [SerializeField] private PlayerController m_Player;

    private int m_Level;

    public event Action<BoosterType> NewBoosterPicked;
    public event Action BoosterExpiredOrCancelled;
    public event Action NewObstacleBroken;
    public event Action PlayerDead;
    public event Action PlayerVictory;
    public event Action PlayerDefeat;

    private const string CURRENT_LEVEL = "CURRENT_LEVEL";

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GameState = GameState.PreGame;

        if (PlayerPrefs.HasKey(CURRENT_LEVEL))
            m_Level = PlayerPrefs.GetInt(CURRENT_LEVEL);
        else
            PlayerPrefs.SetInt(CURRENT_LEVEL, 1);
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject(0))
            return;

        #if UNITY_EDITOR
        if (GameState == GameState.PreGame && Input.GetKeyDown(KeyCode.Space))
            StartGame();
        #else
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began && GameState == GameState.PreGame)
                StartGame();
        }
        #endif
    }

    public void StartGame()
    {
        GameState = GameState.Play;
        UIManager.Instance.HideStartCanvas();
        UIManager.Instance.TogglePlayCanvas(true);
        AdManager.Instance.HideBanner();
    }

    public void KillPlayer()
    {
        GameState = GameState.EndGame;
        HandlePlayerDead();
        UIManager.Instance.TogglePlayCanvas(false);
        AdManager.Instance.ShowBanner();
        UIManager.Instance.DisplayDeadCanvas();
    }

    public void EndGame(int rank)
    {
        GameState = GameState.EndGame;
        Rank = rank;

        UIManager.Instance.TogglePlayCanvas(false);
        AdManager.Instance.ShowBanner();
        GemManager.Instance.CashInGems(rank);

        
        switch (rank)
        {
            case 1:
                m_Level += 1;
                PlayerPrefs.SetInt(CURRENT_LEVEL, m_Level);
                UIManager.Instance.DisplayWinCanvas();
                HandlePlayerVictory();
                break;
            case 2:
                UIManager.Instance.DisplayLoseCanvas();
                HandlePlayerDefeat();
                break;
            case 3:
                UIManager.Instance.DisplayLoseCanvas();
                HandlePlayerDefeat();
                break;
            case 4:
                UIManager.Instance.DisplayLoseCanvas();
                HandlePlayerDefeat();
                break;
        }
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToggleSkinShopGameState(bool status)
    {
        GameState = status ? GameState.SkinShop : GameState.PreGame;
    }

    #region Event handles
    public void HandleNewBoosterPicked(BoosterType booster)
    {
        if (NewBoosterPicked != null)
            NewBoosterPicked(booster);
    }

    public void HandleBoosterExpiredOrCancelled()
    {
        if (BoosterExpiredOrCancelled != null)
            BoosterExpiredOrCancelled();
    }

    public void HandleNewObstacleBroken()
    {
        if (NewObstacleBroken != null)
            NewObstacleBroken();
    }

    public void HandlePlayerDead()
    {
        if (PlayerDead != null)
            PlayerDead();
    }

    public void HandlePlayerVictory()
    {
        if (PlayerVictory != null)
            PlayerVictory();
    }

    public void HandlePlayerDefeat()
    {
        if (PlayerDefeat != null)
            PlayerDefeat();
    }
    #endregion
}

[SerializeField]
public enum GameState
{
    PreGame,
    Play,
    Pause,
    EndGame,
    SkinShop
}