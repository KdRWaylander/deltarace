﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [Header("Collectibles")]
    [SerializeField] private GameObject m_BoosterCrate;
    [SerializeField] [Range(0, 1)] private float m_BoosterCrateProbability = .1f;

    [Header("Waypoints")]
    [SerializeField] private Transform m_WaypointHolderOne;
    [SerializeField] private Transform m_WaypointHolderTwo;
    [SerializeField] private Transform m_WaypointHolerThree;

    private void Start()
    {
        // Handle the booster crate position
        if (Random.value < 1 - m_BoosterCrateProbability)
        {
            m_BoosterCrate.SetActive(false);
        }
        else
        {
            if (transform.position.y <= -6)
                m_BoosterCrate.transform.localPosition = Vector3.up;
            else
                m_BoosterCrate.transform.localPosition = Random.value > .5f ? Vector3.up : Vector3.down;
        }

        // Handle the waypoints positions
        m_WaypointHolderOne.localPosition = Random.value > .5f ? Vector3.up : Vector3.down;
        m_WaypointHolderTwo.localPosition = Random.value > .5f ? Vector3.up : Vector3.down;
        m_WaypointHolerThree.localPosition = Random.value > .5f ? Vector3.up : Vector3.down;
    }

    public Transform GetWaypointHolder(int index)
    {
        if (index == 1)
            return m_WaypointHolderOne;
        else if (index == 2)
            return m_WaypointHolderTwo;
        else
            return m_WaypointHolerThree;
    }
}