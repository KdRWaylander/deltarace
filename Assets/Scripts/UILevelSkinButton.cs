﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevelSkinButton : MonoBehaviour
{
    [SerializeField] private int m_SkinIndex;
    [SerializeField] private int m_UnlockLevel;
    [SerializeField] private Image m_SkinImage;
    [SerializeField] private TextMeshProUGUI m_UnlockLevelText;
    
    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
    }

    private void Start()
    {
        if(m_UnlockLevel > GameManager.Instance.Level)
        {
            m_SkinImage.gameObject.SetActive(false);

            m_UnlockLevelText.text = m_UnlockLevel.ToString();
            m_UnlockLevelText.gameObject.SetActive(true);

            m_Button.interactable = false;
        }
        else
        {
            m_SkinImage.gameObject.SetActive(true);
            m_SkinImage.sprite = SkinManager.Instance.GetSkinPreview(m_SkinIndex);

            m_UnlockLevelText.gameObject.SetActive(false);
        }
    }

    public void SetSkin()
    {
        SkinManager.Instance.SetSkin(m_SkinIndex);
    }
}