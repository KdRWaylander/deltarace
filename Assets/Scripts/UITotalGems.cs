﻿using TMPro;
using UnityEngine;

public class UITotalGems : MonoBehaviour
{
    private TextMeshProUGUI m_Text;

    private void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        UpdateGemsText();
    }

    private void UpdateGemsText()
    {
        m_Text.text = GemManager.Instance.TotalGems.ToString();
    }

    private void OnEnable()
    {
        GemManager.Instance.NewGemsAmount += UpdateGemsText;
    }

    private void OnDisable()
    {
        GemManager.Instance.NewGemsAmount -= UpdateGemsText;
    }
}