﻿using UnityEngine;
using UnityEngine.UI;

public class UIBoosterImage : MonoBehaviour
{
    [SerializeField] private Sprite m_SpeedBoosterSprite;
    [SerializeField] private Sprite m_MagnetBoosterSprite;
    [SerializeField] private Sprite m_DestructionBoosterSprite;
    [SerializeField] private Sprite m_TransparentSprite;

    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        GameManager.Instance.NewBoosterPicked += UpdateBoosterImage;
        GameManager.Instance.BoosterExpiredOrCancelled += ResetBoosterImage;

        ResetBoosterImage();
    }

    private void UpdateBoosterImage(BoosterType booster)
    {
        switch (booster)
        {
            case BoosterType.Speed:
                m_Image.sprite = m_SpeedBoosterSprite;
                break;
            case BoosterType.Magnet:
                m_Image.sprite = m_MagnetBoosterSprite;
                break;
            case BoosterType.Destruction:
                m_Image.sprite = m_DestructionBoosterSprite;
                break;
        }
    }

    private void ResetBoosterImage()
    {
        m_Image.sprite = m_TransparentSprite;
    }

    private void OnDisable()
    {
        GameManager.Instance.NewBoosterPicked -= UpdateBoosterImage;
        GameManager.Instance.BoosterExpiredOrCancelled -= ResetBoosterImage;
    }
}