﻿using System;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager Instance { get; private set; }
    public bool IsVibrationOn { get { return m_IsVibrationOn; } }
    public bool IsSoundOn { get { return m_IsSoundOn; } }

    private bool m_IsVibrationOn;
    private bool m_IsSoundOn;

    public event Action<bool> SoundSettingChanged;
    public event Action<bool> VibrationSettingChanged;

    private const string VIBRATION = "VIBRATION";
    private const string SOUND = "SOUND";

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        // Check vibration status
        if (PlayerPrefs.HasKey(VIBRATION))
        {
            m_IsVibrationOn = (PlayerPrefs.GetInt(VIBRATION) == 1) ? true : false;
        }
        else
        {
            m_IsVibrationOn = true;
            PlayerPrefs.SetInt(VIBRATION, 1);
        }

        // Check sound status
        if (PlayerPrefs.HasKey(SOUND))
        {
            m_IsSoundOn = (PlayerPrefs.GetInt(SOUND) == 1) ? true : false;
        }
        else
        {
            m_IsSoundOn = true;
            PlayerPrefs.SetInt(SOUND, 1);
        }
    }

    public void ToggleVibration()
    {
        m_IsVibrationOn = !m_IsVibrationOn;

        HandleVibrationSettingChanged(m_IsVibrationOn);

        int i = m_IsVibrationOn ? 1 : 0;
        PlayerPrefs.SetInt(VIBRATION, i);
    }

    public void ToggleSound()
    {
        m_IsSoundOn = !m_IsSoundOn;

        HandleSoundSettingChanged(m_IsSoundOn);

        int i = m_IsSoundOn ? 1 : 0;
        PlayerPrefs.SetInt(SOUND, i);
    }

    private void HandleSoundSettingChanged(bool newStatus)
    {
        if (SoundSettingChanged != null)
            SoundSettingChanged(newStatus);
    }

    private void HandleVibrationSettingChanged(bool newStatus)
    {
        if (VibrationSettingChanged != null)
            VibrationSettingChanged(newStatus);
    }
}