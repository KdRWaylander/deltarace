﻿using TMPro;
using UnityEngine;

public class UICurrentLevelText : MonoBehaviour
{
    [SerializeField] private bool m_AddLevelKeyword = true;
    [SerializeField] private bool m_AddOne = false;
    [SerializeField] private bool m_SubstractOne = false;

    private void Start()
    {
        int level;
        if (m_AddOne)
            level = GameManager.Instance.Level + 1;
        else if (m_SubstractOne)
            level = GameManager.Instance.Level - 1;
        else
            level = GameManager.Instance.Level;

        GetComponent<TextMeshProUGUI>().text = m_AddLevelKeyword ?
            "LEVEL " + level.ToString():
            level.ToString();
    }
}