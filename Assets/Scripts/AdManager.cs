﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour, IUnityAdsListener
{
    public static AdManager Instance { get; private set; }
    
    [SerializeField] private bool m_TestMode = true;

    private const string GAME_ID = "3847397";
    private const string PLACEMENT_VIDEO = "video";
    private const string PLACEMENT_BANNER = "banner";
    private const string PLACEMENT_REWARDED_VIDEO = "rewardedVideo";

    private void Awake()
    {
        Instance = this;
    }

    private IEnumerator Start()
    {
        Advertisement.AddListener(this); // Needed with IUnityAdsListener to have access to the callbacks
        Advertisement.Initialize(GAME_ID, m_TestMode);

        while (Advertisement.IsReady(PLACEMENT_BANNER))
            yield return null;

        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(PLACEMENT_BANNER);
    }

    public void ShowBanner()
    {
        Advertisement.Banner.Show(PLACEMENT_BANNER);
    }

    public void HideBanner()
    {
        Advertisement.Banner.Hide();
    }

    public void ShowVideoToReload()
    {
        if (Advertisement.IsReady(PLACEMENT_VIDEO))
            Advertisement.Show(PLACEMENT_VIDEO);
        else
            GameManager.Instance.ReloadGame();
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady(PLACEMENT_REWARDED_VIDEO))
            Advertisement.Show(PLACEMENT_REWARDED_VIDEO);
        else
            VibrationManager.Instance.VibrateSmall();
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // If the ending ad is a video to restart the game, reload
        if (GameManager.Instance.GameState == GameState.EndGame && (placementId == PLACEMENT_REWARDED_VIDEO || placementId == PLACEMENT_VIDEO))
            GameManager.Instance.ReloadGame();

        // If the ad is a rewarded video to add gems, add gems
        if (GameManager.Instance.GameState == GameState.SkinShop && placementId == PLACEMENT_REWARDED_VIDEO && showResult == ShowResult.Finished)
            GemManager.Instance.AddGems(1000);
    }

    public void OnUnityAdsReady(string placementId)
    {
        // Unused callback
    }

    public void OnUnityAdsDidError(string message)
    {
        // Unused callback
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Unused callback
    }
}