﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }
    
    [SerializeField] private AudioClip m_GemAudioClip;
    [SerializeField] private AudioClip m_BoosterAudioClip;
    [SerializeField] private AudioClip m_DestructionAudioClip;
    [SerializeField] private AudioClip m_DeathAudioClip;
    [SerializeField] private AudioClip m_VictoryAudioClip;
    [SerializeField] private AudioClip m_DefeatAudioClip;

    private bool m_IsAudioOn;

    private AudioSource m_AudioSource;

    private void Awake()
    {
        Instance = this;

        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        m_IsAudioOn = SettingsManager.Instance.IsSoundOn;
    }

    private void OnEnable()
    {
        SettingsManager.Instance.SoundSettingChanged += UpdateAudioStatus;
        
        GemManager.Instance.NewGemPicked += PlayGemSound;
        GameManager.Instance.NewBoosterPicked += PlayBoosterSound;
        GameManager.Instance.NewObstacleBroken += PlayDestructionSound;
        GameManager.Instance.PlayerDead += PlayDeathSound;
        GameManager.Instance.PlayerVictory += PlayVictorySound;
        GameManager.Instance.PlayerDefeat += PlayDefeatSound;
    }

    private void UpdateAudioStatus(bool newStatus)
    {
        m_IsAudioOn = newStatus;
    }

    private void PlayGemSound()
    {
        if(m_IsAudioOn)
            m_AudioSource.PlayOneShot(m_GemAudioClip);
    }

    private void PlayBoosterSound(BoosterType booster)
    {
        if (m_IsAudioOn)
            m_AudioSource.PlayOneShot(m_BoosterAudioClip);
    }

    private void PlayDestructionSound()
    {
        if (m_IsAudioOn)
            m_AudioSource.PlayOneShot(m_DestructionAudioClip);
    }

    private void PlayDeathSound()
    {
        if (m_IsAudioOn)
            m_AudioSource.PlayOneShot(m_DeathAudioClip);
    }

    private void PlayVictorySound()
    {
        if (m_IsAudioOn)
            m_AudioSource.PlayOneShot(m_VictoryAudioClip);
    }

    private void PlayDefeatSound()
    {
        if (m_IsAudioOn)
            m_AudioSource.PlayOneShot(m_DefeatAudioClip);
    }

    private void OnDisable()
    {
        SettingsManager.Instance.SoundSettingChanged -= UpdateAudioStatus;

        GemManager.Instance.NewGemPicked -= PlayGemSound;
        GameManager.Instance.NewBoosterPicked -= PlayBoosterSound;
        GameManager.Instance.NewObstacleBroken -= PlayDestructionSound;
        GameManager.Instance.PlayerDead -= PlayDeathSound;
        GameManager.Instance.PlayerVictory -= PlayVictorySound;
        GameManager.Instance.PlayerDefeat -= PlayDefeatSound;
    }
}