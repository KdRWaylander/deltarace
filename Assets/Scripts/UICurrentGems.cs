﻿using TMPro;
using UnityEngine;

public class UICurrentGems : MonoBehaviour
{
    [SerializeField] private bool m_AddPlusAndMultiplierKeywords = false;
    
    private TextMeshProUGUI m_Text;

    private void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        UpdateCurrentCoinText();
    }

    private void OnEnable()
    {
        GemManager.Instance.NewGemPicked += UpdateCurrentCoinText;
    }

    private void UpdateCurrentCoinText()
    {
        if(m_AddPlusAndMultiplierKeywords)
            m_Text.text = "+" + GemManager.Instance.CurrentGems.ToString() + " (x" + GemManager.Instance.MultiplierGems.ToString() + ")";
        else
            m_Text.text = GemManager.Instance.CurrentGems.ToString();
    }

    private void OnDisable()
    {
        GemManager.Instance.NewGemPicked -= UpdateCurrentCoinText;
    }
}