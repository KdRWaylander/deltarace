﻿using UnityEngine;

public class Gem : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() != null)
        {
            PickGem();
        }
    }

    private void PickGem()
    {
        GemManager.Instance.HandleNewGemPicked();
        Destroy(gameObject);
    }
}