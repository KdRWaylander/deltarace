﻿using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public int Index { get { return m_Index; } }
    [SerializeField] private int m_Index;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<RobotController>() != null)
        {
            other.gameObject.GetComponent<RobotController>().SelectNextWaypoint();
        }
    }
}