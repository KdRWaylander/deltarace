﻿using UnityEngine;

public class UIReloadButton : MonoBehaviour
{
    [SerializeField] private bool m_IsEndGameReload = true;

    public void Reload()
    {
        if (m_IsEndGameReload)
            AdManager.Instance.ShowVideoToReload();
        else
            GameManager.Instance.ReloadGame();
    }
}