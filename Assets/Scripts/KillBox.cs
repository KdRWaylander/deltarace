﻿using UnityEngine;

public class KillBox : MonoBehaviour
{
    [SerializeField] private GameObject m_ExplosionFX;
    [SerializeField] private GameObject m_DestructionFX;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerController>() != null)
        {
            if(other.gameObject.GetComponent<PlayerController>().IsUnderDestructionBoost == true)
            {
                other.gameObject.GetComponent<PlayerController>().ResetAllBoost();
                Instantiate(m_DestructionFX, other.transform.position, Quaternion.identity);
                GameManager.Instance.HandleNewObstacleBroken();
                Destroy(gameObject);
            }
            else
            {
                other.gameObject.SetActive(false);
                Instantiate(m_ExplosionFX, other.transform.position, Quaternion.identity);
                GameManager.Instance.KillPlayer();
            }
        }
    }
}