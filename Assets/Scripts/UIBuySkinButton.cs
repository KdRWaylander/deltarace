﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIBuySkinButton : MonoBehaviour
{
    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
    }

    private void Start()
    {
        ToggleInteractabiliy();
    }

    private void OnEnable()
    {
        SkinManager.Instance.NewSkinUnlocked += ToggleInteractabiliy;
        GemManager.Instance.NewGemsAmount += ToggleInteractabiliy;
    }

    public void BuySkin()
    {
        if(SkinManager.Instance.AreAllSkinsUnlocked() == false) // security check
            SkinManager.Instance.UnlockRandomSkin();
    }

    private void ToggleInteractabiliy()
    {
        StartCoroutine(ToggleInteractabiliyCoroutine());
    }

    private void ToggleInteractabiliy(int index)
    {
        ToggleInteractabiliy();
    }

    private IEnumerator ToggleInteractabiliyCoroutine()
    {
        yield return new WaitForEndOfFrame(); // makes sure all Unlocking/PlayerPrefs actions are finished before to check

        if (GemManager.Instance.TotalGems >= 5000 && SkinManager.Instance.AreAllSkinsUnlocked() == false)
            m_Button.interactable = true;
        else
            m_Button.interactable = false;
    }

    private void OnDisable()
    {
        SkinManager.Instance.NewSkinUnlocked -= ToggleInteractabiliy;
        GemManager.Instance.NewGemsAmount -= ToggleInteractabiliy;
    }
}