﻿using System.Collections;
using TMPro;
using UnityEngine;

public class UIBoosterText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_Text;
    [SerializeField] private float m_DisplayTime = 1f;

    private void Start()
    {
        m_Text.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        GameManager.Instance.NewBoosterPicked += DisplayBoosterText;
    }

    private void DisplayBoosterText(BoosterType booster)
    {
        StartCoroutine(DisplayBoosterTextCoroutine(booster));
    }

    private IEnumerator DisplayBoosterTextCoroutine(BoosterType booster)
    {
        m_Text.gameObject.SetActive(true);

        switch (booster)
        {
            case BoosterType.Speed:
                m_Text.text = "Speed boost !";
                break;
            case BoosterType.Magnet:
                m_Text.text = "Magnet boost !";
                break;
            case BoosterType.Destruction:
                m_Text.text = "Destruction boost !";
                break;
        }

        yield return new WaitForSeconds(m_DisplayTime);

        m_Text.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        GameManager.Instance.NewBoosterPicked -= DisplayBoosterText;
    }
}