﻿using System.Collections;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    [SerializeField] private bool m_UseCustomFinishCamera;
    [SerializeField] private Transform m_CustomFinishCameraPositionTransform;

    private int m_FinishedPlayers;

    private void Start()
    {
        m_FinishedPlayers = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() != null)
        {
            m_FinishedPlayers += 1;
            GameManager.Instance.EndGame(m_FinishedPlayers);
            StartCoroutine(DeactivateAfterTime(other.gameObject, .5f));
            FindObjectOfType<CameraController>().ChangeTarget(m_CustomFinishCameraPositionTransform);
        }

        if(other.GetComponent<RobotController>() != null)
        {
            m_FinishedPlayers += 1;
            StartCoroutine(DeactivateAfterTime(other.gameObject, .5f));
        }
    }

    private IEnumerator DeactivateAfterTime(GameObject go, float time)
    {
        yield return new WaitForSeconds(time);

        go.SetActive(false);
    }
}