﻿using UnityEngine;
using UnityEngine.UI;

public class UIPodiumImage : MonoBehaviour
{
    [SerializeField] private Sprite m_Podium2ndPlaceSprite;
    [SerializeField] private Sprite m_Podium3rdPlaceSprite;
    [SerializeField] private Sprite m_Podium4thPlaceSprite;

    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        switch (GameManager.Instance.Rank)
        {
            case 2:
                m_Image.sprite = m_Podium2ndPlaceSprite;
                break;
            case 3:
                m_Image.sprite = m_Podium3rdPlaceSprite;
                break;
            case 4:
                m_Image.sprite = m_Podium4thPlaceSprite;
                break;
            default:
                break;
        }
    }
}