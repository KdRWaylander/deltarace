﻿using UnityEngine;
using UnityEngine.UI;

public class UISoundButton : MonoBehaviour
{
    [SerializeField] private Sprite m_SoundOnSprite;
    [SerializeField] private Sprite m_SoundOffSprite;

    private bool m_IsOn;

    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        UpdateImage();
    }

    public void ToggleSound()
    {
        SettingsManager.Instance.ToggleSound();
        UpdateImage();
    }

    private void UpdateImage()
    {
        m_IsOn = SettingsManager.Instance.IsSoundOn;
        m_Image.sprite = m_IsOn ? m_SoundOnSprite : m_SoundOffSprite;
    }
}