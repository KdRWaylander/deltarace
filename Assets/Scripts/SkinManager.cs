﻿using System;
using UnityEngine;

public class SkinManager : MonoBehaviour
{
    public static SkinManager Instance { get; private set; }

    [SerializeField] private SkinController m_Player;
    [SerializeField] private Skin[] m_Skins;

    public event Action<int> NewSkinUnlocked;

    private const string CURRENT_SKIN = "CURRENT_SKIN";
    private const string GEM_SKIN = "GEM_SKIN_";

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if(PlayerPrefs.HasKey(CURRENT_SKIN) == false)
            PlayerPrefs.SetInt(CURRENT_SKIN, m_Skins[0].index);

        SetSkin(PlayerPrefs.GetInt(CURRENT_SKIN));
    }

    public Sprite GetSkinPreview(int m_SkinIndex)
    {
        foreach(Skin skin in m_Skins)
        {
            if (skin.index == m_SkinIndex)
                return skin.previewSprite;
        }
        return m_Skins[0].previewSprite; // default
    }

    public bool IsSkinUnlocked(int m_SkinIndex)
    {
        if (PlayerPrefs.HasKey(GEM_SKIN + m_SkinIndex.ToString()) == false)
            PlayerPrefs.SetInt(GEM_SKIN + m_SkinIndex.ToString(), 0);
        
        return PlayerPrefs.GetInt(GEM_SKIN + m_SkinIndex.ToString()) == 1;
    }

    public Skin GetRandomSkin()
    {
        return m_Skins[UnityEngine.Random.Range(0, m_Skins.Length)];
    }

    public void SetSkin(int m_SkinIndex)
    {
        foreach (Skin skin in m_Skins)
        {
            if (skin.index == m_SkinIndex)
            {
                m_Player.SetSkin(skin);
                PlayerPrefs.SetInt(CURRENT_SKIN, skin.index);
                return;
            }
        }
        m_Player.GetComponent<SkinController>().SetSkin(m_Skins[0]); // default
        PlayerPrefs.SetInt(CURRENT_SKIN, m_Skins[0].index); // default
    }

    public bool AreAllSkinsUnlocked()
    {
        for(int i = 8; i < m_Skins.Length; i++)  // starts at 8 to skip the skins unlocked with levels
        {
            if (IsSkinUnlocked(m_Skins[i].index) == false)
                return false;
        }
        return true;
    }

    public void UnlockRandomSkin()
    {
        bool isFound = false;
        int i = 0;
        int security = 50; // to break the while loop after a given number of iterations

        while(isFound == false || security < 0)
        {
            i = UnityEngine.Random.Range(8, m_Skins.Length); // starts at 8 to skip the skins unlocked with levels
            if(IsSkinUnlocked(m_Skins[i].index) == false)
            {
                GemManager.Instance.WithdrawGems(5000);
                PlayerPrefs.SetInt(GEM_SKIN + m_Skins[i].index.ToString(), 1);
                HandleNewSkinUnlocked(m_Skins[i].index);
                isFound = true;
            }

            security--;
        }
    }

    public void HandleNewSkinUnlocked(int index)
    {
        if (NewSkinUnlocked != null)
        {
            NewSkinUnlocked(index);
        }
    }
}

[Serializable]
public struct Skin
{
    public int index;
    public Material skin;
    public Sprite previewSprite;
}