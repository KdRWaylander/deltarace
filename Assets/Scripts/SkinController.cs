﻿using UnityEngine;

public class SkinController : MonoBehaviour
{
    public Skin Skin { get { return m_CurrentSkin; } }
    
    [SerializeField] private MeshRenderer m_DeltaplaneRenderer;
    [SerializeField] private SkinnedMeshRenderer m_PilotRenderer;

    private Skin m_CurrentSkin;

    public void SetSkin(Skin skin)
    {
        m_DeltaplaneRenderer.material = skin.skin;
        m_PilotRenderer.material = skin.skin;

        m_CurrentSkin = skin;
    }
}