﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Deltaplane))]
public class RobotController : MonoBehaviour
{
    [SerializeField] private int m_Index;
    [SerializeField] private float m_MinSpeed = 5f;
    [SerializeField] private float m_MaxSpeed = 7.5f;
    [SerializeField] private AnimationCurve m_DifficultyCurve;
    [SerializeField] private float m_RotationSpeed = 5f;
    [SerializeField] private float m_MaxTiltAngle = 65f;
    [SerializeField] private Transform m_Deltaplane;
    
    private float m_Speed;
    private Queue<Transform> m_Waypoints;
    private Transform m_CurrentWaypoint;
    private bool m_IsReadyToFly;

    private SkinController m_SkinController;

    private void Awake()
    {
        m_SkinController = GetComponent<SkinController>();
    }

    private void OnEnable()
    {
        LevelGenerator.Instance.TerrainGenerated += GeneratePath;
    }

    private void Start()
    {
        m_SkinController.SetSkin(SkinManager.Instance.GetRandomSkin());
        m_Speed = Random.Range(m_MinSpeed, m_DifficultyCurve.Evaluate(GameManager.Instance.Level / 100) * m_MaxSpeed);
    }

    private void FixedUpdate()
    {
        if (GameManager.Instance.GameState != GameState.Play || m_IsReadyToFly == false)
            return;

        // move the robot without rotating the parent transform
        transform.position = Vector3.MoveTowards(transform.position, m_CurrentWaypoint.position, m_Speed * Time.fixedDeltaTime);

        // only rotate the transform of the visual part of the robot, in a consistent way player-wise
        Vector3 direction = Vector3.Normalize(m_CurrentWaypoint.position - transform.position);
        float angleToReach = Vector3.SignedAngle(transform.forward, direction, Vector3.right);
        float angleClamped = Mathf.Clamp(angleToReach, 0, m_MaxTiltAngle);
        m_Deltaplane.localRotation = Quaternion.RotateTowards(m_Deltaplane.localRotation, Quaternion.Euler(angleClamped, 0, 0), m_RotationSpeed * Time.fixedDeltaTime);
    }

    public void SelectNextWaypoint()
    {
        if (m_Waypoints.Count > 0)
            m_CurrentWaypoint = m_Waypoints.Dequeue();
    }

    private void GeneratePath(List<Obstacle> obstacles)
    {
        // Gather waypoints
        m_Waypoints = new Queue<Transform>();
        foreach (var obstacle in obstacles)
        {
            m_Waypoints.Enqueue(obstacle.GetWaypointHolder(m_Index).GetChild(0));
            m_Waypoints.Enqueue(obstacle.GetWaypointHolder(m_Index).GetChild(1));
        }
        m_Waypoints.Enqueue(FindObjectOfType<FinishLine>().transform);

        // Select first waypoint
        SelectNextWaypoint();

        m_IsReadyToFly = true;
    }

    private void OnDisable()
    {
        LevelGenerator.Instance.TerrainGenerated -= GeneratePath;
    }
}