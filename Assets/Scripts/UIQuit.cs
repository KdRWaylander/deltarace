﻿using System.Collections;
using UnityEngine;

public class UIQuit : MonoBehaviour
{
    [SerializeField] private float m_WaitTime = 1f;

    private bool m_Quit = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Quit();
    }

    private void Quit()
    {
        if (m_Quit)
            Application.Quit();
        else
            StartCoroutine(QuitAction());
    }

    private IEnumerator QuitAction()
    {
        m_Quit = true;
        yield return new WaitForSeconds(m_WaitTime);
        m_Quit = false;
    }
}