﻿using UnityEngine;

public class Booster : MonoBehaviour
{
    [SerializeField] private float m_SpeedBoostQuantity = 3f;
    [SerializeField] private float m_SpeedBoostDuration = 3f;
    [SerializeField] [Range(0, 1)] private float m_SpeedBoostProbability = .33f;
    [SerializeField] private float m_MagnetBoostDuration = 3f;
    [SerializeField] [Range(0, 1)] private float m_MagnetBoostProbability = .33f;
    [SerializeField] private GameObject m_PickUpFX;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerController>() != null)
        {
            var player = other.gameObject.GetComponent<PlayerController>();
            player.ResetAllBoost();

            float seed = Random.value;
            if (seed <= m_SpeedBoostProbability)
                player.SpeedBoost(m_SpeedBoostQuantity, m_SpeedBoostDuration);
            else if (seed <= m_SpeedBoostProbability + m_MagnetBoostProbability)
                player.MagnetBoost(m_MagnetBoostDuration);
            else
                player.DestructionBoost();

            Instantiate(m_PickUpFX, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}

[SerializeField]
public enum BoosterType
{
    Speed,
    Magnet,
    Destruction
}