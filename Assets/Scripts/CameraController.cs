﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform m_TargetToFollow;
    [SerializeField] private Vector3 m_PositionOffset;
    [SerializeField] private Vector3 m_LookAtOffset;

    private bool m_Follow;

    private void Start()
    {
        m_Follow = true;
    }

    private void FixedUpdate()
    {
        if (m_Follow == false)
            return;

        transform.position = m_TargetToFollow.position + m_PositionOffset;
        transform.LookAt(m_TargetToFollow.position + m_LookAtOffset);
    }

    public void ChangeTarget(Transform newPositionTransform) // Stop following the player and set itself to the custom finish camera position
    {
        transform.position = newPositionTransform.position;
        transform.rotation = newPositionTransform.rotation;

        m_Follow = false;
    }
}