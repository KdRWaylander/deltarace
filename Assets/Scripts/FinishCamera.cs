﻿using UnityEngine;

public class FinishCamera : MonoBehaviour
{
    [SerializeField] private Transform m_CameraPositionTransform;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null)
            FindObjectOfType<CameraController>().ChangeTarget(m_CameraPositionTransform);
    }
}