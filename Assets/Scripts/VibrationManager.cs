﻿using UnityEngine;

public class VibrationManager : MonoBehaviour
{
    public static VibrationManager Instance { get; private set; }

    [SerializeField] private long m_ShortVibrationLength = 100;
    [SerializeField] private long m_LongVibrationLength = 200;

    private bool m_IsVibrationOn;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_IsVibrationOn = SettingsManager.Instance.IsVibrationOn;
    }

    private void OnEnable()
    {
        SettingsManager.Instance.VibrationSettingChanged += UpdateVibrationStatus;

        GemManager.Instance.NewGemPicked += PlayGemVibration;
        GameManager.Instance.NewBoosterPicked += PlayBoosterVibration;
        GameManager.Instance.NewObstacleBroken += PlayDestructionVibration;
        GameManager.Instance.PlayerDead += PlayDeathVibration;
        GameManager.Instance.PlayerVictory += PlayVictoryVibration;
        GameManager.Instance.PlayerDefeat += PlayDefeatVibration;
    }

    public void VibrateSmall()
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void UpdateVibrationStatus(bool newStatus)
    {
        m_IsVibrationOn = newStatus;
    }

    private void PlayGemVibration()
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void PlayBoosterVibration(BoosterType booster)
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void PlayDestructionVibration()
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void PlayDeathVibration()
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void PlayVictoryVibration()
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void PlayDefeatVibration()
    {
        if (m_IsVibrationOn)
            Vibration.Vibrate(m_ShortVibrationLength);
    }

    private void OnDisable()
    {
        SettingsManager.Instance.VibrationSettingChanged -= UpdateVibrationStatus;

        GemManager.Instance.NewGemPicked -= PlayGemVibration;
        GameManager.Instance.NewBoosterPicked -= PlayBoosterVibration;
        GameManager.Instance.NewObstacleBroken -= PlayDestructionVibration;
        GameManager.Instance.PlayerDead -= PlayDeathVibration;
        GameManager.Instance.PlayerVictory -= PlayVictoryVibration;
        GameManager.Instance.PlayerDefeat -= PlayDefeatVibration;
    }
}