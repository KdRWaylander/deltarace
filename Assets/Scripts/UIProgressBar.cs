﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UIProgressBar : MonoBehaviour
{
    private float m_LevelLength;
    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        m_LevelLength = LevelGenerator.Instance.LevelLength;
        m_Image.fillAmount = 0;
    }

    private void Update()
    {
        m_Image.fillAmount = GameManager.Instance.Player.transform.position.z / m_LevelLength;
    }
}