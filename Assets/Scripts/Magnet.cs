﻿using UnityEngine;

public class Magnet : MonoBehaviour
{
    [SerializeField] private float m_Stength = 10f;
    [SerializeField] private float m_Radius = 2f;

    private void FixedUpdate()
    {
        foreach(Collider collider in Physics.OverlapSphere(transform.position, m_Radius))
        {
            if(collider.GetComponent<Gem>() != null)
                collider.transform.Translate(Vector3.Normalize(transform.position - collider.transform.position) * m_Stength * Time.fixedDeltaTime);
        }
    }
}