﻿using UnityEngine;
using UnityEngine.UI;

public class UIVibrationButton : MonoBehaviour
{
    [SerializeField] private Sprite m_VibrationOnSprite;
    [SerializeField] private Sprite m_VibrationOffSprite;

    private bool m_IsOn;

    private Image m_Image;

    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        UpdateImage();
    }

    public void ToggleVibration()
    {
        SettingsManager.Instance.ToggleVibration();
        UpdateImage();
    }

    private void UpdateImage()
    {
        m_IsOn = SettingsManager.Instance.IsVibrationOn;
        m_Image.sprite = m_IsOn ? m_VibrationOnSprite : m_VibrationOffSprite;
    }
}